#include "contiki.h"
#include "coap-engine.h"
#include "dev/leds.h"

#include <string.h>

#if PLATFORM_HAS_LEDS || LEDS_COUNT

/* Log configuration */

#include "sys/log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL LOG_LEVEL_APP

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void res_event_handler(void);

extern uint8_t led;
int strpos;

EVENT_RESOURCE(res_led,
         "title=\"LEDs: ?color=red|green|yellow; POST/PUT mode=on|off\";rt=\"Control\"",
         res_get_handler,
         NULL,
         NULL,
         NULL,
         res_event_handler);

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
  coap_set_header_content_format(response,TEXT_PLAIN);

     if(led == LEDS_RED) {
       strpos = snprintf((char *)buffer,preferred_size,"RED");
     }else if(led == LEDS_YELLOW){
       strpos = snprintf((char *)buffer,preferred_size,"YELLOW");
     }else if(led == LEDS_GREEN){
       strpos = snprintf((char *)buffer,preferred_size,"GREEN");
     }

    leds_off(LEDS_ALL);
    leds_on(LEDS_NUM_TO_MASK(led));

  coap_set_payload(response,buffer,strpos);
}

static void res_event_handler(void){
    coap_notify_observers(&res_led);
}

#endif /* PLATFORM_HAS_LEDS */
