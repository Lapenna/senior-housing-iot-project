#include "contiki.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "coap-engine.h"

#include "config-parameters.h"

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL LOG_LEVEL_APP

static void res_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

extern coap_resource_t res_temperature;

extern bool active_air_cond;
extern float temperature;
int strpos;

//used by every node to know type of node
RESOURCE(res_air_conditioner,
         "title=\"set air_conditioner\"",
         res_get_handler,
         res_post_handler,
         NULL,
         NULL);

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
    coap_set_header_content_format(response,TEXT_PLAIN);
    if (active_air_cond){
        strpos = snprintf((char *)buffer,preferred_size,"ON");
    } else{
        strpos = snprintf((char *)buffer,preferred_size,"OFF");
    }
    coap_set_payload(response,buffer,strpos);
}
static void res_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
    const char *value = NULL;
    if(coap_get_query_variable(request, "value", &value)) {
        int temp = atoi(value);
        printf("%d\n", temp);
        // 0 --> OFF
        if(temp == 0){
            active_air_cond = false;
        }else if (temp >= MIN_TEMP && temp <= MAX_TEMP){
            temperature = temp;
            active_air_cond = true;
        }
        res_temperature.trigger();
        coap_set_status_code(response,CREATED_2_01);
    }else{
        coap_set_status_code(response, BAD_REQUEST_4_00);
    }
}
