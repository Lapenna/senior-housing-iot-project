package it.unipi.iot.coap;

import it.unipi.iot.coap.nodes.House;
import it.unipi.iot.coap.nodes.INode;
import it.unipi.iot.coap.nodes.Node;
import it.unipi.iot.coap.nodes.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.californium.core.*;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.server.resources.CoapExchange;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CoAPResource extends CoapResource {

    public static final String HOUSE = "house";
    public static final String PERSON = "person";


    private static Logger LOGGER = LogManager.getLogger(CoAPResource.class);
    private HashMap<String, INode> nodes = new HashMap<>();
    private int houseN = 0;
    private int personN = 0;

    public CoAPResource(String name) {
        super(name);
        setObservable(true);
    }

    public HashMap<String, INode> getNodes() {
        return nodes;
    }

    public void handleGET(CoapExchange exchange) {

        try {
            Response response = new Response(CoAP.ResponseCode.CONTENT);
            saveNode(exchange.advanced().getRequest());

            if (exchange.getRequestOptions().getAccept() == MediaTypeRegistry.APPLICATION_XML) {
                response.getOptions().setContentFormat(MediaTypeRegistry.APPLICATION_XML);
                response.setPayload("<value>Connect</value>");
            } else if (exchange.getRequestOptions().getAccept() == MediaTypeRegistry.APPLICATION_JSON) {
                response.getOptions().setContentFormat(MediaTypeRegistry.APPLICATION_JSON);
                response.setPayload("{\"value\":\"connect\"}");
            } else {
                response.setPayload("connect");
            }

            exchange.respond(response);
        } catch (Exception e) {
            LOGGER.error(e);
        }

    }

    public void postRequest(INode node, String value, String resourceName) {
        Request request = new Request(CoAP.Code.POST);
        Resource resource = node.getResource(resourceName);
        if (resource == null) {
            LOGGER.error("Res don't exist");
            return;
        }
        request.getOptions().addUriQuery("value=" + value);

        resource.getClientResource().advanced(new CoapHandler() {
            public void onLoad(CoapResponse response) {
            }

            public void onError() {
            }
        }, request);
    }

    private CoapClient createCoapClient(Request request, String resource) {
        return new CoapClient(getURI(request, resource));
    }

    private String getName(CoapClient client) {
        CoapResponse response = client.get();
        if (response != null) {
            return response.getResponseText();
        }
        return null;
    }

    private String getURI(Request request, String resource) {
        return "coap://[" + request.getSource().getHostAddress() + "]:" + request.getSourcePort() + resource;
    }

    private List<Resource> getResources(Request request) {

        List<Resource> resources = new ArrayList<>();

        CoapClient client = createCoapClient(request, Node.CORE);
        String stringResources = client.get().getResponseText();

        String[] res = stringResources.split(",");

        for (String re : res) {
            resources.add(new Resource(re.replace("<", "").replace(">", "").substring(0, re.indexOf(";") - 2)));
        }

        return resources;
    }

    private void saveNode(Request request) {
        String uri = getURI(request, Node.NAME);

        if (nodes.containsKey(uri)) { //check if node is already registered
            return;
        }

        CoapClient client = createCoapClient(request, Node.NAME);
        String name = getName(client);

        // gets the resources as array
        List<Resource> resources = getResources(request);

        LOGGER.info("Start to register a " + name + " node with URI: " + uri);
        System.out.print("Start to register a " + name + " node with URI: " + uri);
        System.out.flush();

        switch (Objects.requireNonNull(name)) {
            case HOUSE:

                House house = new House(uri);

                LOGGER.info("Get a House node");

                //set resource on house node
                setResources(house, resources, request);

                houseN++;
                house.setName("house" + houseN);
                nodes.put(house.getURI(), house);
                break;

            case PERSON:
                Person person = new Person(uri);

                LOGGER.info("Get a Person node");

                //set resource on person node
                setResources(person, resources, request);

                personN++;
                person.setName("person" + personN);
                nodes.put(person.getURI(), person);
                LOGGER.info("person " + person.getURI() + " is registered");
                break;
        }

    }

    private void setResources(INode node, List<Resource> resources, Request request) {

        for (Resource resource : resources) {

            LOGGER.info("Elaborate resource: " + resource.getName());

            // creates the CoapClient instance associated with this resource
            CoapClient resourceClient = createCoapClient(request, resource.getName());

            resource.setClientResource(resourceClient);

            CoapObserveRelation relation = resourceClient.observe(
                    new CoapHandler() {
                        public void onLoad(CoapResponse response) {
                            resource.value = response.getResponseText();
                            LOGGER.info("content resource is: " + resource.value);
                        }

                        public void onError() {
                            LOGGER.error("FAILED!!");
                        }
                    });

            try {
                Thread.sleep(1 * 1000);
            } catch (InterruptedException e) {
                LOGGER.error(e);
            }

            node.addResource(resource);

        }
    }

}
