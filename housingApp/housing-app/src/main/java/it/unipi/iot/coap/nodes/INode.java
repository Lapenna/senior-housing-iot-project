package it.unipi.iot.coap.nodes;

import it.unipi.iot.coap.Resource;

public interface INode {

    void showContent();

    String getName();

    void setName(String name);

    void addResource(Resource resource);

    void modifyResource(String resourceName, String value);

    String getURI();

    Resource getResource(String resourceName);

}
