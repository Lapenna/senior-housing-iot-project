package it.unipi.iot.coap.nodes;

public class Person extends Node implements INode {

    public static String BUTTON = "/button";
    public static String MESSAGE = "/mess";
    public static String BUTTONOK = "OK";
    public static String BUTTONRESET = "RESET";

    public Person(String uri) {
        this.URI = uri;
    }
}
