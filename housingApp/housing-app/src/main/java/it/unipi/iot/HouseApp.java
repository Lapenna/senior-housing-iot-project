package it.unipi.iot;

import it.unipi.iot.coap.CoAPResource;
import it.unipi.iot.coap.Resource;
import it.unipi.iot.coap.nodes.House;
import it.unipi.iot.coap.nodes.INode;
import it.unipi.iot.coap.nodes.Person;
import org.eclipse.californium.core.CoapServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HouseApp extends CoapServer {

    private static Logger LOGGER = LogManager.getLogger(HouseApp.class);

    public static void main(String[] args) {

        LOGGER.info("Start server!\n");
        HouseApp houseApp = new HouseApp();
        CoAPResource coAPResource = new CoAPResource("register");
        houseApp.add(coAPResource);
        houseApp.start();

        Thread client = new Thread(() -> {
            HashMap<String, String> nameToUri = new HashMap<>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String command;
            HashMap<String, Resource> resources = new HashMap<>();

            System.out.print("Start Houses administration\n");
            System.out.flush();

            while (true) {
                command = null;

                System.out.print("House avaliable are: \n\n");
                for (INode node : coAPResource.getNodes().values()) {
                    if (node instanceof House) {
                        System.out.print(" House:" + node.getName() + "\n");
                        nameToUri.put(node.getName(), node.getURI());
                        node.showContent();
                    }
                }

                System.out.print("Person avaliable are: \n\n");
                for (INode node : coAPResource.getNodes().values()) {
                    if (node instanceof Person) {
                        System.out.print(" Person:" + node.getName() + "\n");
                        nameToUri.put(node.getName(), node.getURI());
                        node.showContent();
                    }
                }

                System.out.print("Chose node, or digit 0 for exit, or 9 tu update results: ");
                System.out.flush();
                try {
                    command = reader.readLine();
                } catch (IOException e) {
                    LOGGER.error(e);
                }
                if (Objects.requireNonNull(command).equals("9")) {
                    continue;
                }
                if (Objects.requireNonNull(command).equals("0")) {
                    break;
                }


                String uri = nameToUri.get(command);
                INode node = coAPResource.getNodes().get(uri);

                if (node instanceof House) {
                    System.out.print("You can chose the temperature(0 --> TURN OFF): ");
                    System.out.flush();
                    try {
                        command = reader.readLine();
                        if (Integer.parseInt(command) == 0) {
                            node.modifyResource(House.AIRCOND, "OFF");
                        } else {
                            node.modifyResource(House.AIRCOND, "ON");
                        }
                        coAPResource.postRequest(node, command, House.AIRCOND);

                    } catch (IOException e) {
                        LOGGER.error(e);
                    }

                } else if (node instanceof Person) {
                    System.out.print("You are many posibility:\n");
                    System.out.print(" 1)You can send a message\n");
                    System.out.print(" 2)Reset button\n");
                    System.out.print(" chose operation: ");
                    System.out.flush();
                    try {
                        command = reader.readLine();
                        switch (Integer.parseInt(command)) {

                            case 1:
                                System.out.print("Insert message:");
                                command = reader.readLine();
                                node.modifyResource(Person.MESSAGE, command);
                                coAPResource.postRequest(node, command, Person.MESSAGE);
                                break;

                            case 2:
                                node.modifyResource(Person.BUTTON, Person.BUTTONOK);
                                coAPResource.postRequest(node, Person.BUTTONRESET, Person.BUTTON);
                                break;
                        }

                    } catch (IOException e) {
                        LOGGER.error(e);
                    }
                } else {
                    System.out.print("Node not valid chose an other command\n");
                }

                System.out.flush();

            }
        });
        client.start();

    }
}
