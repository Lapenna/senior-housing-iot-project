#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_


// Set the maximum number of CoAP concurrent transactions:

#undef COAP_MAX_OPEN_TRANSACTIONS
#define COAP_MAX_OPEN_TRANSACTIONS 10
#undef COAP_MAX_OBSERVERS
#define COAP_MAX_OBSERVERS 10

#define LOG_LEVEL_APP LOG_LEVEL_DBG

#endif /* PROJECT_CONF_H_ */
