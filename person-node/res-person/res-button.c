#include "contiki.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "coap-engine.h"
#include "os/dev/button-hal.h"

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void res_event_handler(void);
static void res_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);


int strpos;
extern bool btn;

EVENT_RESOURCE(res_button,
    "title=\"Emergency button\"",
    res_get_handler,
    res_post_handler,
    NULL,
    NULL,
    res_event_handler);

static void res_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
      const char *value = NULL;
      if(coap_get_query_variable(request, "value", &value)) {
        if (strncmp(value,"RESET",strlen("RESET")) == 0){
          btn = false;
        }
          coap_set_status_code(response,CREATED_2_01);
      }else{
          coap_set_status_code(response, BAD_REQUEST_4_00);
      }
  }

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
    coap_set_header_content_format(response, TEXT_PLAIN);
    if (btn) {
      printf("button was pushed\n" );
      strpos = snprintf((char *)buffer,preferred_size,"EMERGENCY");
    }else{
      strpos = snprintf((char *)buffer,preferred_size,"OK");
    }
    coap_set_payload(response,buffer,strpos);
}

static void res_event_handler(void){
    coap_notify_observers(&res_button);
}
